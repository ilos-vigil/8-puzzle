# 8-puzzle

* Requirement :
  * .NET Framework 4.7
  * Visual Studio 2017
  * Windows OS

## Screenshot

### Open Application

![Screenshot Idle](img/idle.png)

### Solving 8-puzzle

![Screenshot Processing](img/processing.png)

### Explore progress

![Screenshot Complete](img/complete.png)