﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_puzzle {
    class BFS : Search {
        public Stack<TreeNode> path;

        public BFS(int[,] cube, int[,] cube_goal) : base(cube, cube_goal) {
            TreeNode head = new TreeNode(start, 0);
            bool goal = false;
            Queue<TreeNode> OPEN = new Queue<TreeNode>();
            OPEN.Enqueue(head);
            Stack<TreeNode> CLOSED = new Stack<TreeNode>();
            while (!goal && OPEN.Count != 0) {
                TreeNode stateX = OPEN.Dequeue();
                CLOSED.Push(stateX);
                //stateX.data.debug();
                if (stateX.data.compare(goal_puzzle)) {
                    goal = true;
                } else {
                    List<TreeNode> successor = stateX.data.GetAllSolutions();
                    List<TreeNode> toBeRemoved = new List<TreeNode>();
                    //filter
                    foreach (var item in successor) {
                        foreach (var closed in CLOSED) {
                            if (item.data.compare(closed.data)) {
                                toBeRemoved.Add(item);
                                //successor.Remove(item);
                                break;
                            }
                        }
                    }
                    foreach (var item in toBeRemoved) {
                        successor.Remove(item);
                    }
                    foreach (var item in successor) {
                        item.parent = stateX;
                        //item.data.debug();
                        OPEN.Enqueue(item);
                    }
                }
            }

            if (goal) {
                Console.WriteLine("hore ketemu");
                Console.WriteLine("CLOSED count : {0}", CLOSED.Count);
                /*
                foreach (var item in CLOSED) {
                    item.data.debug();
                }
                */

                path = getPath(CLOSED);
            } else {
                Console.WriteLine("Gx nEMU");
            }
        }
    }
}
