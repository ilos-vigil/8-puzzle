﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_puzzle {
    public class PriorityQueue<T> {
        public List<TreeNode> data;

        public PriorityQueue() {
            this.data = new List<TreeNode>();
        }
        public void addItem(int isi, TreeNode item) {
            item.cost = isi;
            data.Add(item);
            int child = data.Count - 1;
            while (child > 0) {
                int parent = child - 1;
                if (data[child].cost > data[parent].cost)
                    break;
                TreeNode tmp = data[child];
                data[child] = data[parent];
                data[parent] = tmp;
                child = parent;
            }

        }
        public TreeNode takeItem() {
            TreeNode firstItem = data[0];
            /*
            Console.WriteLine(data[0].data.cube[0, 0] + " " + data[0].data.cube[0, 1] + " " + data[0].data.cube[0, 2]);
            Console.WriteLine(data[0].data.cube[1, 0] + " " + data[0].data.cube[1, 1] + " " + data[0].data.cube[1, 2]);
            Console.WriteLine(data[0].data.cube[2, 0] + " " + data[0].data.cube[2, 1] + " " + data[0].data.cube[2, 2] + "\n");
            Console.WriteLine(data[0].cost);
            */
            
            for (int i = 0; i < data.Count() - 1; i++) {
                data[i] = data[i + 1];
            }
            return firstItem;
        }
        public int length() {
            return data.Count();
        }
    }
}
