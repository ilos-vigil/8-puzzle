﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_puzzle {

    public class EightPuzzle {
        //[3,3]
        public int[,] cube;
        //supaya tidak perlu foreach untuk cari posisi blank
        public int blankX;
        public int blankY;
        //lebar X,Y
        public int ARRAY_LIMIT = 0;
        public int GRID_X = 3;
        public int GRID_Y = 3;


        public EightPuzzle(int[,] cube,int posBlankX,int posBlankY) {
            this.cube = cube;
            this.blankX = posBlankX;
            this.blankY = posBlankY;
        }

        public EightPuzzle getNewInstance() {
            //buat dapetin child tree
            //return new EightPuzzle();
            return null;
        }

        public bool compare(EightPuzzle target) {
            //Compare 2 puzzle misal sama berarti problem solve
            if(blankX != target.blankX &&  blankY != target.blankY)
            {
                //optimisasi
                return false;
            }
            for (int y = 0; y < 3; y++) {
                for (int x = 0; x < 3; x++) {
                    if(cube[y, x] != target.cube[y, x] ){
                        return false;
                    }
                }
            }

            return true;
        }
        public List<TreeNode> GetAllSolutions(){
            List<TreeNode> solutions = new List<TreeNode>();
            int[] checkY = new int[]{-1,1,0,0};
            int[] checkX = new int[]{0,0,-1,1};
            
            for(int i=0;i<4;i++){
                if(blankX + checkX[i] <3 && blankX + checkX[i] >=0 &&  blankY + checkY[i] >=0 && blankY + checkY[i] <3){
                    //Console.WriteLine("============ PARENT============");
                    //debug();
                    //Console.WriteLine("============================");
                    int[,] temp =  new int[3,3];
                    Array.Copy(this.cube, temp,this.cube.Length);
                    //tambah solusi
                    EightPuzzle solutionPuzzle = new EightPuzzle(temp, this.blankX, this.blankY);
                    TreeNode newSolution = new TreeNode(solutionPuzzle,0);
                    newSolution.data.move(checkX[i], checkY[i]);
                    //TODO implementasi depth
                    //newSolution.depth = 0;
                    solutions.Add(newSolution);
                }
            }
            return solutions;
            //return solutions.ToArray();
        }
        public void move(int arahX,int arahY)
        {
            //swap value
            int nextValue= cube[blankY + arahY, blankX + arahX];
            cube[blankY, blankX] = nextValue;
            cube[blankY + arahY, blankX + arahX] = 0;
            //assign new blank position
            blankY = blankY + arahY;
            blankX = blankX + arahX;
        }

        public void debug()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(cube[i,j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        // also can be used to check child ?
        public bool isParent(EightPuzzle parent) {
            int[] checkY = new int[] { -1, 1, 0, 0 };
            int[] checkX = new int[] { 0, 0, -1, 1 };

            // 1. check if 7 of 9 number in same position
            int ctr = 0;
            for (int y = 0; y < 3; y++) {
                for (int x = 0; x < 3; x++) {
                    if(cube[y,x] == parent.cube[y,x]){
                        ctr++;
                    }
                }
            }

            if (ctr != 7)
                return false;

            // 2. check X position
            for (int i = 0; i < 4; i++) {
                if (blankX + checkX[i] == parent.blankX && blankY + checkY[i] == parent.blankY) {
                    return true;
                }
            }

            return false;
        }
    }
}
