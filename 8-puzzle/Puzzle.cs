﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_puzzle
{
    public class Puzzle
    {
        private int[,] puzzle = new int[3,3];

        public Puzzle(int[,] puzzle)
        {
            this.puzzle = puzzle;
        }
        
        //SETTER
        public void setValueIndex(int x, int y, int value)
        {
            this.puzzle[x, y] = value;
        }

        //GETTER
        public int getValueIndex(int x, int y)
        {
            return this.puzzle[x, y];
        }

    }
}
