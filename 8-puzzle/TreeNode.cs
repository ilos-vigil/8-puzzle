﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_puzzle {
    public class TreeNode {
        //public TargetPuzzle parent;
        //public TargetPuzzle[] children;
        public int depth;
        public EightPuzzle data;
        public TreeNode parent;
        public int cost; // used for A*

        public TreeNode(EightPuzzle data,int depth)
        {
            this.data = data;
            this.depth = depth;
            this.parent = null;
            this.cost = 0;
        }
    }
}
