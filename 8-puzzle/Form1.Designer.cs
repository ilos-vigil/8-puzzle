﻿namespace _8_puzzle {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.rdBFS = new System.Windows.Forms.RadioButton();
            this.rdDFS = new System.Windows.Forms.RadioButton();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.rdAS = new System.Windows.Forms.RadioButton();
            this.lbIndex = new System.Windows.Forms.Label();
            this.nudIndex = new System.Windows.Forms.NumericUpDown();
            this.lbStart = new System.Windows.Forms.Label();
            this.lbGoal = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudIndex)).BeginInit();
            this.SuspendLayout();
            // 
            // rdBFS
            // 
            this.rdBFS.AutoSize = true;
            this.rdBFS.Checked = true;
            this.rdBFS.Location = new System.Drawing.Point(306, 215);
            this.rdBFS.Name = "rdBFS";
            this.rdBFS.Size = new System.Drawing.Size(51, 24);
            this.rdBFS.TabIndex = 2;
            this.rdBFS.TabStop = true;
            this.rdBFS.Text = "BFS";
            this.rdBFS.UseVisualStyleBackColor = true;
            // 
            // rdDFS
            // 
            this.rdDFS.AutoSize = true;
            this.rdDFS.Location = new System.Drawing.Point(384, 215);
            this.rdDFS.Name = "rdDFS";
            this.rdDFS.Size = new System.Drawing.Size(53, 24);
            this.rdDFS.TabIndex = 3;
            this.rdDFS.Text = "DFS";
            this.rdDFS.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(312, 245);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 34);
            this.btnStart.TabIndex = 4;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnBack
            // 
            this.btnBack.Enabled = false;
            this.btnBack.Location = new System.Drawing.Point(189, 367);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(46, 34);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "<";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.Location = new System.Drawing.Point(572, 367);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(46, 34);
            this.btnNext.TabIndex = 7;
            this.btnNext.Text = ">";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(415, 245);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 34);
            this.btnReset.TabIndex = 8;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // rdAS
            // 
            this.rdAS.AutoSize = true;
            this.rdAS.Location = new System.Drawing.Point(457, 215);
            this.rdAS.Name = "rdAS";
            this.rdAS.Size = new System.Drawing.Size(43, 24);
            this.rdAS.TabIndex = 9;
            this.rdAS.Text = "A*";
            this.rdAS.UseVisualStyleBackColor = true;
            // 
            // lbIndex
            // 
            this.lbIndex.AutoSize = true;
            this.lbIndex.Location = new System.Drawing.Point(624, 374);
            this.lbIndex.Name = "lbIndex";
            this.lbIndex.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbIndex.Size = new System.Drawing.Size(44, 20);
            this.lbIndex.TabIndex = 10;
            this.lbIndex.Text = "n / m";
            // 
            // nudIndex
            // 
            this.nudIndex.Enabled = false;
            this.nudIndex.Location = new System.Drawing.Point(628, 331);
            this.nudIndex.Name = "nudIndex";
            this.nudIndex.Size = new System.Drawing.Size(120, 27);
            this.nudIndex.TabIndex = 11;
            this.nudIndex.ValueChanged += new System.EventHandler(this.nudIndex_ValueChanged);
            // 
            // lbStart
            // 
            this.lbStart.AutoSize = true;
            this.lbStart.Location = new System.Drawing.Point(155, 60);
            this.lbStart.Name = "lbStart";
            this.lbStart.Size = new System.Drawing.Size(40, 20);
            this.lbStart.TabIndex = 12;
            this.lbStart.Text = "Start";
            // 
            // lbGoal
            // 
            this.lbGoal.AutoSize = true;
            this.lbGoal.Location = new System.Drawing.Point(605, 60);
            this.lbGoal.Name = "lbGoal";
            this.lbGoal.Size = new System.Drawing.Size(40, 20);
            this.lbGoal.TabIndex = 13;
            this.lbGoal.Text = "Goal";
            // 
            // lbStatus
            // 
            this.lbStatus.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbStatus.AutoSize = true;
            this.lbStatus.Location = new System.Drawing.Point(302, 177);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(85, 20);
            this.lbStatus.TabIndex = 14;
            this.lbStatus.Text = "Status : Idle";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 489);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.lbGoal);
            this.Controls.Add(this.lbStart);
            this.Controls.Add(this.nudIndex);
            this.Controls.Add(this.lbIndex);
            this.Controls.Add(this.rdAS);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.rdDFS);
            this.Controls.Add(this.rdBFS);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(812, 528);
            this.MinimumSize = new System.Drawing.Size(812, 528);
            this.Name = "Form1";
            this.Text = "8-Puzzle";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudIndex)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton rdBFS;
        private System.Windows.Forms.RadioButton rdDFS;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.RadioButton rdAS;
        private System.Windows.Forms.Label lbIndex;
        private System.Windows.Forms.NumericUpDown nudIndex;
        private System.Windows.Forms.Label lbStart;
        private System.Windows.Forms.Label lbGoal;
        private System.Windows.Forms.Label lbStatus;
    }
}

