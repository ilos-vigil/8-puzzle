﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _8_puzzle {
    public partial class Form1 : Form {

        Puzzle target, game;
        TextBox[,] targetKotak, gameKotak, pathKotak;

        int[,] defaultStart, defaultGoal;

        DFS dfs;
        BFS bfs;
        A_star a_star;
        TreeNode[] path; // untuk menunjukan progress
        int pathCtr = 0;

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //DECLARE DEFAULT
            defaultGoal = new int[3, 3];
            defaultGoal[0, 0] = 1;
            defaultGoal[0, 1] = 2;
            defaultGoal[0, 2] = 3;
            defaultGoal[1, 0] = 8;
            defaultGoal[1, 1] = 0;
            defaultGoal[1, 2] = 4;
            defaultGoal[2, 0] = 7;
            defaultGoal[2, 1] = 6;
            defaultGoal[2, 2] = 5;

            defaultStart = new int[3, 3];
            defaultStart[0, 0] = 1;
            defaultStart[0, 1] = 2;
            defaultStart[0, 2] = 3;
            defaultStart[1, 0] = 0;
            defaultStart[1, 1] = 7;
            defaultStart[1, 2] = 4;
            defaultStart[2, 0] = 6;
            defaultStart[2, 1] = 8;
            defaultStart[2, 2] = 5;

            //DECLARE TEXTBOX
            targetKotak = new TextBox[3, 3];
            gameKotak = new TextBox[3, 3];
            pathKotak = new TextBox[3, 3];

            //CETAK KOTAK KOSONG
            //KOTAK GAME
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    TextBox kotak = new TextBox();
                    kotak.Parent = this;
                    kotak.Multiline = true;
                    kotak.Size = new Size(50, 50);
                    kotak.Location = new Point(100 + (j * 50), 100 + (i * 50));
                    kotak.Visible = true;
                    kotak.ForeColor = Color.Black;
                    // kotak.Text = "";
                    kotak.Text = defaultStart[i,j].ToString();
                    kotak.TextAlign = HorizontalAlignment.Center;
                    gameKotak[i, j] = kotak;
                }
            }

            //KOTAK TARGET
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    TextBox kotak = new TextBox();
                    kotak.Parent = this;
                    kotak.Multiline = true;
                    kotak.Size = new Size(50, 50);
                    kotak.Location = new Point(550 + (j * 50), 100 + (i * 50));
                    kotak.Visible = true;
                    kotak.ForeColor = Color.Black;
                    // kotak.Text = "";
                    kotak.Text = defaultGoal[i, j].ToString();
                    kotak.TextAlign = HorizontalAlignment.Center;
                    targetKotak[i, j] = kotak;
                }
            }

            // KOTAK LOG
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    TextBox kotak = new TextBox();
                    kotak.Parent = this;
                    kotak.Multiline = true;
                    kotak.Size = new Size(50, 50);
                    kotak.Location = new Point(325 + (j * 50), 325 + (i * 50));
                    kotak.Visible = true;
                    kotak.ForeColor = Color.Black;
                    kotak.Text = "";
                    kotak.TextAlign = HorizontalAlignment.Center;
                    kotak.Enabled = false;
                    pathKotak[i, j] = kotak;
                }
            }
        }

        public void removeTextBox() {
            //HAPUS KOTAK LAMA
            for (int ix = this.Controls.Count - 1; ix >= 0; ix--) {
                if (this.Controls[ix] is TextBox)
                    this.Controls[ix].Dispose();
            }
        }

        public void resetGame()
        {
            lbStatus.Text = "Status : Idle";
            lbIndex.Text = "n / m";

            //ENABLE KOTAK FALSE
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    targetKotak[i, j].Text = defaultGoal[i,j].ToString();
                    gameKotak[i, j].Text = defaultStart[i, j].ToString();
                    pathKotak[i, j].Text = "";

                    targetKotak[i, j].Enabled = true;
                    gameKotak[i, j].Enabled = true;
                    pathKotak[i, j].Enabled = false;
                }
            }

            //DISABLE BUTTON
            btnStart.Enabled = true;
            btnBack.Enabled = false;
            btnNext.Enabled = false;

            nudIndex.Enabled = false;

            
        }

        public bool cekKotakTerisi()
        {
            //CEK KOTAK TERISI
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if ((targetKotak[i, j].Text == "" || targetKotak[i, j].Text == null) &&
                        (gameKotak[i, j].Text == "" || gameKotak[i, j].Text == null))
                        return false;
                }
            }
            return true;
        }

        public void showCurrentPath(){
            if(path.Length!=0) {
                lbIndex.Text = pathCtr + " / " + (path.Count() - 1);
                nudIndex.Value = pathCtr;

                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        pathKotak[i, j].Text = path[pathCtr].data.cube[i, j].ToString();
                    }
                }
            }
        }

        public void enablePathViewComponent() {
            lbStatus.Text = "Status : Complete";
            btnReset.Enabled = true;

            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    pathKotak[i, j].Enabled = true;
                }
            }

            nudIndex.Minimum = 0;
            nudIndex.Maximum = path.Count() - 1;
            nudIndex.Enabled = true;

            pathCtr = 0;

            btnBack.Enabled = true;
            btnNext.Enabled = true;
        }

        public async void runAlgorithm(int[,] cube, int[,] cube_goal){
            if (rdDFS.Checked) {
                Task<DFS> task = new Task<DFS>(() => {
                    dfs = new DFS(cube, cube_goal);
                    return dfs;
                });
                task.Start();
                await task;
                path = task.Result.path.ToArray();
            }else if (rdBFS.Checked) {
                Task<BFS> task = new Task<BFS>(() => {
                    bfs = new BFS(cube, cube_goal);
                    return bfs;
                });
                task.Start();
                await task;
                path = task.Result.path.ToArray();
            } else if(rdAS.Checked) {
                Task<A_star> task = new Task<A_star>(() => {
                    a_star = new A_star(cube, cube_goal);
                    return a_star;
                });
                task.Start();
                await task;
                path = task.Result.path.ToArray();
            }

            enablePathViewComponent();
            showCurrentPath();
        }

        public void startGame() {
            bool kotakTerisiSemua = cekKotakTerisi();

            if (kotakTerisiSemua) {
                lbStatus.Text = "Status : Processing";
                btnReset.Enabled = false;

                //ENABLE KOTAK FALSE
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        targetKotak[i, j].Enabled = false;
                        gameKotak[i, j].Enabled = false;
                        pathKotak[i, j].Enabled = false;
                    }
                }

                //DISABLE START BUTTON
                btnStart.Enabled = false;

                //SET ISI CLASS
                int[,] arrTarget = new int[3, 3];
                int[,] arrGame = new int[3, 3];

                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        arrTarget[i, j] = Convert.ToInt32(targetKotak[i, j].Text.ToString());
                        arrGame[i, j] = Convert.ToInt32(gameKotak[i, j].Text.ToString());
                    }
                }

                target = new Puzzle(arrTarget);
                game = new Puzzle(arrGame);

                runAlgorithm(arrGame, arrTarget);
            } else {
                MessageBox.Show("Semua Kotak Harus Terisi");
            }
        }

        private void nudIndex_ValueChanged(object sender, EventArgs e) {
            pathCtr = (int) nudIndex.Value;
            showCurrentPath();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            startGame();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            resetGame();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if(pathCtr - 1 >= 0){
                pathCtr--;
                showCurrentPath();
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if(pathCtr + 1 < path.Length){
                pathCtr++;
                showCurrentPath();
            }
        }
    }
}
