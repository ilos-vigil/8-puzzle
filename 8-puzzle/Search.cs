﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_puzzle {
    // class for State Space Search
    class Search {
        public int[,] cube_goal;
        public GoalPuzzle goal_puzzle;
        
        public int[,] cube;
        public EightPuzzle start;
        public int startX;
        public int startY;
        public int goalX;
        public int goalY;

        public Search(int[,] cube, int[,] cube_goal){
            /*
            cube_goal = new int[3, 3];
            cube_goal[0, 0] = 1;
            cube_goal[0, 1] = 2;
            cube_goal[0, 2] = 3;
            cube_goal[1, 0] = 8;
            cube_goal[1, 1] = 0;
            cube_goal[1, 2] = 4;
            cube_goal[2, 0] = 7;
            cube_goal[2, 1] = 6;
            cube_goal[2, 2] = 5;
            */
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (cube_goal[i, j] == 0)
                    {
                        goalX = j;
                        goalY = i;
                    }
                    if (cube[i, j] == 0)
                    {
                        startX = j;
                        startY = i;
                    }
                }
            }
            goal_puzzle = new GoalPuzzle(cube_goal, goalX, goalY);

            //blank di isi 0
            //https://www.cs.cmu.edu/~eugene/teach/ai00/sample/index.html
            /*
            cube = new int[3, 3];
            cube[0, 0] = 1;
            cube[0, 1] = 2;
            cube[0, 2] = 3;
            cube[1, 0] = 0;
            cube[1, 1] = 7;
            cube[1, 2] = 4;
            cube[2, 0] = 6;
            cube[2, 1] = 8;
            cube[2, 2] = 5;
            */
            start = new EightPuzzle(cube, startX, startY);
        }

        public void debug(int[,] cube) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    Console.Write(cube[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public Stack<TreeNode> getPath(Stack<TreeNode> CLOSED, bool checkParent = true) {
            Stack<TreeNode> correctNode = new Stack<TreeNode>(); // shallow copy

            while (CLOSED.Count != 0) {
                TreeNode nodeCLOSED = CLOSED.Pop();

                if (correctNode.Count == 0) {
                    correctNode.Push(nodeCLOSED);
                } else {
                    TreeNode currentCorrectNode = correctNode.Peek();

                    if(checkParent){
                        if(currentCorrectNode.parent == nodeCLOSED){
                            correctNode.Push(nodeCLOSED);
                        }
                    }else{
                        if (currentCorrectNode.data.isParent(nodeCLOSED.data)) {
                            correctNode.Push(nodeCLOSED);
                        }
                    }
                }
            }

            Console.WriteLine("Path count : {0}", correctNode.Count);
            /*
            foreach (var item in correctNode) {
                item.data.debug();
            }
            */

            return correctNode;
        }
    }
}
