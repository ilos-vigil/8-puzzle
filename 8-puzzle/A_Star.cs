﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_puzzle {
    class A_star : Search {
        public Stack<TreeNode> path;

        public A_star(int[,] cube, int[,] cube_goal) : base(cube, cube_goal) {
            TreeNode head = new TreeNode(start, 0);
            bool goal = false;
            PriorityQueue<TreeNode> OPEN = new PriorityQueue<TreeNode>();
            int cost = compareNode(head, goal_puzzle) + head.depth;
            OPEN.addItem(cost, head);
            Stack<TreeNode> CLOSED = new Stack<TreeNode>();
            while (!goal && OPEN.length() != 0) {
                TreeNode stateX = OPEN.takeItem();
                /*
                Console.WriteLine(stateX.data.cube[0, 0] + " " + stateX.data.cube[0, 1] + " " + stateX.data.cube[0, 2]);
                Console.WriteLine(stateX.data.cube[1, 0] + " " + stateX.data.cube[1, 1] + " " + stateX.data.cube[1, 2]);
                Console.WriteLine(stateX.data.cube[2, 0] + " " + stateX.data.cube[2, 1] + " " + stateX.data.cube[2, 2]);
                */
                CLOSED.Push(stateX);
                //stateX.data.debug();
                if (stateX.data.compare(goal_puzzle)) {
                    goal = true;
                } else {
                    List<TreeNode> successor = stateX.data.GetAllSolutions();
                    List<TreeNode> toBeRemoved = new List<TreeNode>();
                    //filter
                    foreach (var item in successor) {
                        foreach (var closed in CLOSED) {
                            if (item.data.compare(closed.data)) {
                                toBeRemoved.Add(item);
                                //successor.Remove(item);
                                break;
                            }
                        }
                    }
                    foreach (var item in toBeRemoved) {
                        successor.Remove(item);
                    }
                    foreach (var item in successor) {
                        item.parent = stateX;
                        //item.data.debug();
                        OPEN.addItem(compareNode(item, goal_puzzle) + item.depth, item);
                    }
                }
            }

            if (goal) {
                Console.WriteLine("hore ketemu");
                Console.WriteLine("CLOSED count : {0}", CLOSED.Count);
                /*
                foreach (var item in CLOSED) {
                    item.data.debug();
                }
                */

                path = getPath(CLOSED);
            } else {
                Console.WriteLine("Gx nEMU");
            }
        }

        public int compareNode(TreeNode now, GoalPuzzle goal) {
            int costPath = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    costPath += searchInGoal(now.data.cube[j, i], j, i);
                }
            }
            return costPath;
        }

        public int searchInGoal(int key, int x, int y) {
            int costNode = 0;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (goal_puzzle.cube[j, i] == key) {
                        costNode += Math.Abs(i - y) + Math.Abs(j - x);
                    }
                }
            }
            return costNode;
        }
    }
}
